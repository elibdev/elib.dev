# Eli Bierman's Dev Blog

My personal site. 

## Developing

This site is built using [VitePress][vitepress].

[vitepress]: https://vitepress.vuejs.org/

Run `yarn` to install the dependencies, and `yarn docs:dev` to start the dev server.

Run `yarn docs:build` to build the static site which will be output to `docs/.vitepress/dist/`.

For more info on using VitePress, check out the [VitePress tutorial][vitepress-intro] I wrote based on my experience building this site.

[vitepress-intro]: https://www.elib.dev/vitepress-intro/
