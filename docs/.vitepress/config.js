module.exports = {
    lang: "en-US",
    title: "Eli Bierman ☕️",
    description: "I'm Eli Bierman, a full-stack developer based in NYC.",
    themeConfig: {
        sidebar: false,
        nav: [
            { text: "Code", link: "#code" },
            { text: "Writing", link: "#writing" },
            { text: "Get In Touch", link: "#get-in-touch" },
        ],
    },
};
