# Introduction to Vitepress

_Last updated: May 4, 2021_

[[toc]]

## Vitepress Background

[Vitepress][vitepress] is a [Vue][vue]-powered static site generator built on [Vite][vite], created by Evan You.
It is still in early stages, but it is already highly capable and fun to work with.

If you want a more mature option you should check out [Vuepress][vuepress],
which is the predecessor to Vitepress and currently has more functionality.

Vitepress is perfect for creating documentation sites because you can create a website just by writing Markdown files with no configuration needed.
You can gradually write Vue components as needed to display content that you can use directly in the Markdown.

## Vite Makes Iterating Fast

There a few cool features that come directly from Vite:

- Vue single-file components work with no configuration
- hot reloading dev server so you can see your edits instantly
- simplified configuration inherited from Vite when needed

[vitepress]: https://vitepress.vuejs.org/
[vite]: https://vitejs.dev/
[vue]: https://vuejs.org/
[vuepress]: https://vuepress.vuejs.org/

With most static-site generators there's a delay between editing your site and seeing the results in the browser.
As your site grows in complexity, the delay to see the results increases.

Because Vite is so fast, it unlocks a different workflow where you can have your code and your browser preview side-by-side, and the browser automatically reloads, while preserving the frontend's state.

It might sound small, but I think once you try it you'll appreciate it.

## Create a New Vitepress Project

Set up a new git repository for your website and add the following to a file named `package.json`.

```json
{
    "scripts": {
        "docs:dev": "vitepress dev docs",
        "docs:build": "vitepress build docs",
        "docs:serve": "vitepress serve docs"
    },
    "devDependencies": {
        "vitepress": "^0.13.2"
    }
}
```

Then run `npm install` to install Vitepress.
Run `npm run docs:dev` to start the Vitepress server.
Now we're ready to add some content!

## Add a Home Page Using the Default Theme

You can add metadata about your pages by writing some YAML between two triple dashes called [frontmatter][frontmatter].
Vitepress's default theme home page accepts some special variables in the frontmatter that we'll use to customize the display.

[frontmatter]: https://vitepress.vuejs.org/guide/frontmatter.html

Create a folder in the root of your project named `docs`.
This is where your content will live.

Create a file at `docs/index.md` with the following content:

```md
---
home: true
heroText: My Awesome Website
tagline: You know it's awesome when it's built on Vitepress.
actionText: Contact
actionLink: /#contact
features:
  - title: Markdown
    details: This site is written in Markdown.
  - title: Vue Components
    details: This site can use custom Vue components.
  - title: It Just Works
    details: You don't need to do much to get a nice site.
footer: Copyright © 2021 [Your Name]
---
## Content

This is where your **content** goes.

You can link to [other pages](/my-page).

## Contact

You can contact me at [contact@example.com](mailto:contact@example.com).
```

Open `http://localhost:3000` in the browser to view our site.
Now when you edit and save `docs/index.md` the page will immediately update to show the new content.

## Customize the Navigation Bar and Sidebar

Vitepress uses a [configuration file][vitepress-conf] that returns a JavaScript object to let you configure site-wide settings and theme-specific settings.

[vitepress-conf]: https://vitepress.vuejs.org/guide/configuration.html

Create a new file named `docs/.vitepress/config.js` with the following content:

```js
module.exports = {
    // The language of your website's content
    lang: "en-US",
    // Your website's title
    title: "Vitepress Demo",
    // Your website's description (this will usually show in search results)
    description: "This website is a demonstration of Vitepress.",
    // This section configures the default Vitepress theme.
    themeConfig: {
        // We are turning the sidebar off here, but if you want to keep it,
        // it accepts the same type of objects as nav (with `text` and `link` property)
        sidebar: false,
        // This configures the sticky navigation bar at the top of the page.
        nav: [
            { text: "Content", link: "#content" },
            { text: "Contact", link: "#contact" },
        ],
    },
};
```

The navigation bar should update when you save the file.

## Customize the Default Theme Colors & Fonts

While Vitepress allows you to create a totally [custom theme][theme], we'll just customize the default theme.

[theme]: https://vitepress.vuejs.org/guide/customization.html

Create a new folder `docs/.vitepress/theme/` where we'll configure the default theme.

Add the following code to `docs/.vitepress/theme/vars.css`:

```css
:root {
  --c-brand: darkred;
  --c-brand-light: red;
  --font-family-base: Georgia, serif;
}
```

This sets CSS variables that the default theme uses to customize fonts and colors.

In order for our site to import these CSS variables, we'll need to create a new theme that uses the default theme with our CSS variables imported.

Add the following code to `docs/.vitepress/theme/index.js`:

```js
import DefaultTheme from 'vitepress/theme'
import './vars.css'

export default {
  ...DefaultTheme
}
```

Whatever CSS variables we declare in `vars.css` will be applied to the whole site.

## Add a New Page

You can add a new page by adding a file named `docs/[page-name].md` and writing your page contents as Markdown in the file.

Add the following to a new file named `docs/my-page.md`

~~~md
# My Page

This is a normal page.

## Code Samples

You can include code samples with syntax highlighting.

```js
const hi = () => {
  console.log("hi")
}
hi()
```

~~~

Now open `http://localhost:3000/my-page` in the browser and you should see the page.

## Use Clean URLs

If you noticed, the full page URL is `http://localhost:3000/my-page.html`.
If you want the path to not end with `.html`, you can create clean URLs by creating a folder named `docs/my-page/` and putting the markdown in `docs/my-page/index.md`.

After you make that change, you can use `http://localhost:3000/my-page/` as your page's full URL. The ending slash is required, since it's technically a directory.

## Add a Custom Vue Component

Vitepress lets you create Vue components that you can use anywhere in your pages' Markdown content to customize how your page contents are displayed.

We'll create a component for displaying posts, with a title, description, and border.

Create a folder named `components` in the root project directory and add the following to `components/Post.vue`:

```html
<script setup>
// Here we're using the composition API with the <script setup> feature.
// Everything in this script runs in the Vue component's setup function and
// any declared variables can be accessed from the component's template.

// defineProps lets you access the component's props from the setup function.
import { defineProps } from 'vue'

// Set the props as variables so we can access them in the template.
const { title, link } = defineProps(['title', 'link'])
</script>

<template>
<div class="post">
  <a :href="link"><p class="title">{{ title }}</p></a>
  <p class="description"><slot></slot></p>
</div>
</template>

<style scoped>
.post {
  border: 3px solid #333;
  margin: 1rem;
  padding: 3rem;
  border-radius: 3rem;
}
.title {
  font-size: 3rem;
  font-weight: 400;
}
.description {
  font-size: 1.5rem;
  font-weight: 300;
}
</style>
```

Now add the following to the end of `docs/index.md`:

```html
<script setup>
import Post from '../components/Post.vue'
</script>

<post title="Vue Wikipedia" link="https://en.wikipedia.org/wiki/Vue.js">
This first post is just a placeholder for a Vue post.
</post>

<post title="JavaScript Wikipedia" link="https://en.wikipedia.org/wiki/JavaScript">
The second post is also a placeholder for a JavaScript post.
</post>
```

Open `http://localhost:3000` in your browser and you should see both posts displayed at the end of the page.

## Deploy Your Static Site

Run the following command to build your static site:

```sh
npm run docs:build
```

The files in `docs/.vitepress/dist/` can be deployed to any static website hosting service.

Congratulations on creating a website with Vitepress!
