# 5 Lessons Developers Can Take from Apple’s Walled Garden

Learning how to develop for Apple’s software ecosystem (notably iOS) requires a significant investment of time and effort. I believe you can learn to build better experiences by understanding the platform deeply. But even if you don’t want to commit that time, you can still learn from Apple. In this post I’ll share with you 5 lessons that you can take from Apple’s approach to growing a software ecosystem:

1. Take the best ideas and integrate them holistically.
2. You are only as healthy as the ecosystem you exist within.
3. Performance is critical to a good user experience.
4. Physical metaphors make interfaces more usable.
5. An actionable design system can scale.

## Take the Best Ideas and Integrate Them Holistically

Apple takes what they think are the best ideas about software, and integrates them into a holistic system that developers can build on. Sometimes it takes them a while, because they want to make sure it all fits together. They won’t release something until they are confident in its quality, since as soon as developers extend it they will need to support it.

In the 1980s they took ideas about graphical user interfaces from Xerox PARC, and more recently they developed Swift by taking ideas from modern programming languages.

The foundation of the software that currently lives as iOS, macOS, watchOS, and tvOS is [directly descended from the NeXTSTEP operating system and development environment,](http://www.computerhistory.org/atchm/the-deep-history-of-your-apps-steve-jobs-nextstep-and-early-object-oriented-programming/) initially released in 1989.

It’s rare for software to last for 30 years, and a development platform that stays relevant that long has a level of maturity and stability that is hard to find.

## You Are Only as Healthy as The Ecosystem You Exist Within

Apple exists in a symbiotic relationship with an ecosystem of designers and developers that build on Apple platforms.

Apple knows that users will only perceive their devices as valuable if they provide value in their lives and are enjoyable to use.

People’s demands for technology are greater than Apple alone can provide, so Apple needs to persuade third-party makers to build apps to extend their platform.

Apple provides composable abstractions around hardware and software components, as well as an integrated developer environment through Xcode, Interface Builder, and Playgrounds to help developers to build and maintain applications more easily.

## Performance is Critical to a Good User Experience

In 2013 I began working at a small startup called Fly Labs building mobile video editing apps. We needed great performance to perform realtime video rendering while keeping the interface snappy and responsive.

The built-in Apple frameworks provided us all the performance we needed to create a mobile video editing experience that was actually productive. We were able to build our apps using low-level libraries like Core Video and Core Audio that were originally designed for desktop video editing software.

It was incredible to hear from people all over the world with a newfound passion for creating videos, saying it had never felt so easy before. We were only able to build such a smooth experience because Apple has a commitment to performance across their platform.

## Physical Metaphors Make Interfaces More Usable

Building a great mobile experience is about creating an illusion of a real physical and tactile experience through software. Smooth animations, transitions, and accurate gesture recognition are essential for this.

Our phones are personal. People are extremely perceptive about interface delays on mobile. A confusing or unresponsive mobile interface deeply disorients users. Such an experience may prompt them to delete your app entirely.

Building a convincing physical metaphor on mobile involves coordinating gesture-based animations with software-based physics without any noticeable lag. Having support for all those things in a way that work together on iOS makes it easy to experiment with new interaction models.

## An Actionable Design System Can Scale

If you want to build a seamless experience, you must think holistically. The user shouldn’t have to think about your app or interface. It should feel completely natural and match their expectations.

People have internalized expectations on how apps are structured, including:

- How interface elements are supposed to look and behave
- How you should be able to navigate within and between apps
- Where similar interface elements should appear

The people at Apple have thought hard about all the different components that go into creating consistent and seamless experiences on their platform. They publish the [Human Interface Guidelines](https://developer.apple.com/design/human-interface-guidelines/) to communicate those ideas.

But a design system is only as good as it is useful. Apple also offers a customizable set of interface components through UIKit that already follow their design guidelines, which developers can easily build their apps on. This encourages platform-wide consistency, and therefore better usability.

## You Can Only Develop For The Latest Software If People Use It

Your digital product doesn’t exist in a vacuum. It exists in a broader ecosystem of technology and experiences, and the user doesn’t really care where one app ends and another begins. Apple controlling the hardware and software means that most users are actually running the latest software. You can have more confidence in the experience your users will actually have.

With the web and Android, you have very few guarantees about the software or hardware that your application might run on, which adds significant development complexity.

Apple may require you to update your app for it to work with a new iOS version. This isn’t because Apple hates developers, it’s because they love users more. Your app will only break if it breaks the consistent experience that Apple demands. It can be painful for developers, but it’s only done to avoid passing that pain onto its users.
