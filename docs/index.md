---
home: true
# heroImage: /logo.png
# heroAlt: Logo image
heroText: Eli Bierman ☕️
tagline: I'm Eli, a full-stack developer in NYC.
actionText: Get In Touch
actionLink: /#get-in-touch
features:
  - title: Backend Development
    details: I have built production backend services in Python, Go, Elixir, and TypeScript. I love designing APIs and documenting them well.
  - title: Frontend Development
    details: I've built web frontends in Vue, React, and ClojureScript, but semantic HTML and accessibility is at every good web frontend's core.
  - title: iOS Development
    details: I'm proficient in Swift, Objective-C, UIKit, and I'm learning SwiftUI. Ask me about my iOS code that lives in the Google Graveyard.
  - title: Payments Experience
    details: Many services I've worked on have been payments-related, and I enjoy the challenges that come with building resilient payment systems.
  - title: Writing & Teaching
    details: I've written technical blog posts, articles covering the creative coding scene in NYC, documentation for open source projects, and taught workshops on iOS, web development, and git.
  - title: Product & UX
    details: On small high-impact teams, I've recruited and onboarded team members, conducted user interviews, written product roadmaps, and delivered great products.
footer: Copyright © 2021 Eli Bierman
---

<script setup>
import Project from '../components/Project.vue'
</script>

## Code

I have built a few open source projects:

<Project title="depo" docs="https://hexdocs.pm/depo/readme.html" code="https://gitlab.com/elibdev/depo" description="a library that provides a simplified API for using SQLite in Elixir" />

<Project title="savior" docs="https://pypi.org/project/savior/" code="https://gitlab.com/elibdev/savior" description="an experimental immutable NoSQL database library in Python using LMDB" />

<Project title="formal.studio" docs="https://formal.studio/how-to" code="https://gitlab.com/elibdev/formal.studio/" description="a markdown-based CMS built using a custom Elixir web framework" />

<Project title="xtal" docs="https://gitlab.com/elibdev/xtal" code="https://gitlab.com/elibdev/xtal/-/blob/main/xtal/xtal.c" description="a unified CLI tool written in C for managing multiple git and fossil repositories" />

## Writing

I wrote about my ideas on [how to write great documentation](/great-docs/).

Drawing from my experience as an iOS developer, I wrote a post sharing some [lessons developers can learn from Apple](/apple-lessons/).

I also wrote tutorials on [setting up a modern Python project](/python-project/) and [building a static site with Vitepress.](/vitepress-intro/)

I also wrote some articles about creative coding in NYC for [Vice Creator's Project][vice].

[vice]: https://www.vice.com/en/contributor/eli-bierman

## Get In Touch

Get in touch by writing to [eli@elib.dev](mailto:eli@elib.dev) and I'd be happy to chat.
