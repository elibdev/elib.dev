# How to Write Documentation

_Last updated: July 18, 2020_

If you want your code to be useful to other people, you need to document it.
In this post I'll share what I think you need to make your documentation useful.

## Code Talks to Computers, Docs Talk to People

Code can never tell you why it was written or how to use it.
People require communication outside of code to understand the social context within which it exists and has purpose.
Documentation is the practice of writing the social context down so you can communicate it to people at scale and facilitate successful adoption of your code.
Another way to think about documentation is as the interface between your code and other people.

## But Nobody Likes Writing Documentation

If you feel this way you are not alone. Writing documentation doesn't always feel like a priority when you also need to code. Good documentation makes code more reusable and understandable, so the code and other coders will thank you for it. Writing documentation at the same time as you write code can make it a more manageable and enjoyable task.

## Writing Docs Helps You

Writing documentation is a great way to think through the details of a technical subject. It can help reframe issues in your mind that lead to insights about the project or areas for improvement that you may not have thought of before. It can even help in the planning stages of a project or new feature. Some people even practice [Documentation-Driven Development](https://gist.github.com/zsup/9434452)!

## Writing Docs Helps the Community

Documentation is the first place most coders go to try out or learn about a new project, or when they come across an issue or try solving a new problem with a project they already are familiar with. That means that documentation contributions can make even more of a positive impact on a project's community than contributions to the code.

[GitHub's 2017 Open Source Survey](http://opensourcesurvey.org/2017/) found that the biggest problem encountered in open source was "incomplete or confusing documentation." Their top insight from the survey was this:

> Documentation is highly valued, frequently overlooked, and a means for establishing inclusive and accessible communities.

Any contribution (small or large) is always greatly appreciated by the project maintainers and its users.

## Aspirational Documentation

Some projects with documentation that I try to learn from:

- [VueJS](https://vuejs.org/v2/guide/)
- [SQLite](https://sqlite.org/quickstart.html)
- [FreeBSD](https://www.freebsd.org/docproj/) (especially compared with Linux)
- [Bunjil](https://bunjil.js.org) (I haven't used it, but this GraphQL server by [@ojkelly](https://dev.to/ojkelly) that he shared in his post has really #nicedocs)
- [Docker](https://docs.docker.com/) (contributed by [@presto412](https://dev.to/presto412))
- [AWS](https://aws.amazon.com/documentation/) (contributed by [@technologymop](https://dev.to/technologymop))

All of these projects include the components I describe below. Let me know in the comments if there are any projects with documentation you love. I'm always looking for more documentation inspiration!

## My Ideal Documentation

In my opinion the ideal documentation usually has 3 components.

1. **The Why / Goals:** the context and goals of the project
2. **The What / API / Reference:** detailed technical documentation of the programming interface
3. **The How / Examples / Guides:** example-based guides for accomplishing specific tasks

### The Why / Goals

- What was the motivation for this project being built in the first place?
- What are some similar projects and how are they different?
- What types of projects would this project be a good fit for, and when might something else be better?

Usually this is best answered by the authors of the project. If it's possible, it might be helpful to get their point of view on this to add to the documentation if it's not there already. Sometimes people leave out explaining where the project might _not_ be the best fit, but it is very useful and appreciated by users.

### The What / API / Reference

- What are the different high-level components and how do they fit together?
- What are the different low-level data types and functions and what do they do?

There is usually some of this already. If there isn't any, it can be very difficult to get started at all. This can be a good place for suggesting changes where things are unclear to you as you are learning how to use the project. Maintainers can sometimes be picky about this area of the documentation since it's viewed as the authoritative source of information, but it can be a good opportunity to learn about the nitty-gritty details of the project.

The format of this type of documentation is often language-specific, since most languages come with some kind of built-in system for generating docs from comments in the source code. That is usually the format that people coding in that language expect to see this type of documentation in.

### The How / Examples / Guides

- How do you install the project and get some basic code running?
- What steps do you take to build a simple application using this project?
- What steps do you take to address common problems the project addresses?

This is usually the area with the most room for improvement and easiest for someone new to the project to jump into.

This part of the documentation holds a user's hand and walks them through each step in a clear way, leading them to the nirvana of working code that solves a real issue (or it's clear how it can be applied to one).

You can write this type of documentation by building a small thing using the project and take careful notes at every step about what you're doing, so that somebody else could follow along by just copy-pasting. You can easily turn this into a guide that takes users from total beginner to a small win and an aha-moment.

Most people coming to a project are total beginners in it, so writing for other beginners from a beginner's perspective is immensely valuable. People with more experience in the project may actually have trouble seeing from a beginner's perspective so they usually really appreciate this type of contribution. I think this type of documentation goes a long way in making projects more approachable.
