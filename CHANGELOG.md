# Changelog

## To Do

- write guide to python fundamentals
- write guide to backend development with python
- write guide to intro to data analysis with pandas and numpy
- write expose on reaktor
- write essay about my experience being nonbinary as a white male-presenting person
- make a "what's your programming language personality?" quiz

## Done

- add mailing list setup with convertkit
- add google analytics
- publish site on netlify
- create home page

